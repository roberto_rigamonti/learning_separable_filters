function [] = low_rank_filter_approx(varargin)
%  low_rank_filter_approx  Approximates a 2D filter bank with a separable filter
%                          bank. For more details please refer to [1].
%
%  Synopsis:
%     low_rank_filter_approx(resume_fb_number)
%
%  Input:
%     resume_fb_number (optional) = number of iteration corresponding to the
%                                   filter bank to resume (it is the number that
%                                   can be read in the filters_txt/ directory)
%
%  [1] R. Rigamonti, A. Sironi, V. Lepetit, and P. Fua, "Learning Separable
%      Filters", CVPR 2013
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

addpath('nrrd');
addpath(genpath('helper_scripts'));

numvarargs = length(varargin);
if (numvarargs>1)
    error('Too many inputs, the only supplemental parameter allowed is, eventually, the fb number to resume');
else
    if (numvarargs==1)
        resume_fb_number = varargin{1};
    else
        resume_fb_number = 0;
    end
end

p = get_config(resume_fb_number);   % <--- MODIFY HERE the algorithm's parameters

[p,original_filters] = load_original_fb(p);
M = init_filters(p,resume_fb_number);
% M is the new, separable filter bank. Each filter is a column of M.

count = resume_fb_number*p.steps_no+1;
sampling_prob = ones(p.original_fb_no,1);
rows_no = p.filters_size;
cols_no = p.filters_size;

while (true)
    % Pick a random filter to approximate (the badly-approximated ones are the
    % ones that will be pick the most).
    rand_filter_n = randsample(p.original_fb_no,1,true,sampling_prob);
    x = original_filters(:,:,rand_filter_n);
    x = x(:);
    
    t = optimize_coeffs(p,M,x);
    
    % Optimize filters using gradient descent
    res = x-M*t;
    M_grad = -2*res*t'*p.filters_grad_step_size;
    M_grad(:,p.filters_no+1:end) = 0;
    M = M-M_grad;
    
    M = apply_componentwise_NN_proximal_operator(p,M);
    M = normalize_fb(p,M);
    
    % Re-evaluate probabilities
    if (rem(count,50)==0)
        fprintf('  Iteration %d, evaluating sampling probabilities\n',count);
        [sampling_prob] = eval_sampling_prob(p,M,original_filters);
    end
    
    if (rem(count,p.steps_no)==0)
        fprintf('  Storing data on disk\n');
        nrrdSave(sprintf('%s/fb_%06d.nrrd',p.filters_img_directory,floor(count/p.steps_no)),reshape_result_fb_as_img(p,M,size(M,2))');
        save_filter_bank(p,M,count);
        save_reconstructed_filter_bank(p,M,count,original_filters);
    end
    count = count+1;
end

end

