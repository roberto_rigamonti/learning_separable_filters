function [sampling_prob] = eval_sampling_prob(p,M,original_filters)
%  eval_sampling_prob  Evaluate the goodness of the representation that can be
%                      obtained given the current separable filter bank.
%
%  Synopsis:
%     [sampling_prob] = eval_sampling_prob(p,M,original_filters)
%
%  Input:
%     p                = simulation's parameters
%     M                = separable filter bank used in the approximation
%     original filters = filters to approximate
%  Output:
%     sampling_prob = pdf that forces the system to sample more
%                     badly-represented filters                     
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

sampling_prob = zeros(p.original_fb_no,1);

for i_x = 1:p.original_fb_no
    x = original_filters(:,:,i_x);
    x = x(:);
    
    t = best_optimize_coeffs(p,M,x,1e-7);
    
    rec = M*t;
    sampling_prob(i_x) = norm(x-rec);
end

fprintf('    Average reconstruction error: %f\n',sum(sampling_prob)/p.original_fb_no);

end

