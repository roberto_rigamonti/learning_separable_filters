function [t] = best_optimize_coeffs(p,M,x,tol)
%  best_optimize_coeffs  Optimize the coefficients of the representation of the
%                        filter given the current separable filter bank until
%                        the improvement falls below a given tolerance.
%
%  Synopsis:
%     [t] = best_optimize_coeffs(p,M,x,tol)
%
%  Input:
%     p   = simulation's parameters
%     M   = separable filter bank used in the approximation
%     x   = filter to approximate
%     tol = improvement tolerance threshold
%  Output:
%     t = coefficients used in the approximation
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

% Initialize coeffs
t = M'*x;
dim = length(t);

improvement = inf;
last_err = 0;
it_no = 1;

% Optimize coeffs
while (improvement>tol)
    res = x-M*t;
    if (it_no>1)
        improvement = abs(last_err-norm(res)/dim);
    end
    last_err = norm(res)/dim;
    
    grad_coeffs = -2*M'*res*p.gradient_step_size;
    t = t-grad_coeffs;
    
    % ST on coeffs
    t(abs(t)<p.lambda_l1) = 0;
    t(t>=p.lambda_l1) = t(t>=p.lambda_l1)-p.lambda_l1;
    t(t<=-p.lambda_l1) = t(t<=-p.lambda_l1)+p.lambda_l1;
    it_no = it_no+1;
end

end
