function [t] = optimize_coeffs(p,M,x)
%  optimized_coeffs  Optimize the coefficients of the representation of the
%                    filter given the current separable filter bank.
%
%  Synopsis:
%     [t] = optimize_coeffs(p,M,x)
%
%  Input:
%     p = simulation's parameters
%     M = separable filter bank used in the approximation
%     x = filter to approximate
%  Output:
%     t = coefficients used in the approximation
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

rows_no = p.filters_size;
cols_no = p.filters_size;
% Initialize coeffs
t = M'*x;
    
for i_gd = 1:p.gradient_steps_no
    res = x-M*t;
        
    grad_coeffs = -2*M'*res*p.gradient_step_size;
    t = t-grad_coeffs;
        
    % ST on coeffs
    t(abs(t)<p.lambda_l1) = 0;
    t(t>=p.lambda_l1) = t(t>=p.lambda_l1)-p.lambda_l1;
    t(t<=-p.lambda_l1) = t(t<=-p.lambda_l1)+p.lambda_l1;
end

end

