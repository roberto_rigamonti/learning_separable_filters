function [M] = normalize_fb(p,M)
%  normalize_fb  Normalize the separable filter bank given as parameter
%
%  Synopsis:
%     [M] = normalize_fb(p,M)
%
%  Input:
%     p = simulation's parameters
%     M = filter bank to normalize
%  Output:
%     M = normalized filter bank
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

for i_f = 1:p.filters_no
    M(:,i_f) = M(:,i_f)/norm(M(:,i_f));
end

end
