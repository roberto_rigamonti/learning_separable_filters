The results are dumped in a directory whose path depends on the choices for the
different parameters (such as dataset name, filter bank name, ...).
The results directory contains several subdirectories:

- analytical: analytical, per-image evaluation of Rand Index and Variation of
              Information
- AVG_stats: average performance, measured for all the considered metrics, over
             the whole test set and the given number of random iterations (THIS
             is usually what you need to consider, see below for a detailed
             explanation)
- PR: per-image precision/recall curves
- responses: responses obtained by the classifier, in NRRD format
- ROC: per-image Receiver Operating Characteristic curves
- test_data: features extracted for the test images in a format suitable for the
             chosen classifier
- test_labels: per-pixel labels for the chosen test images
- train_data: per-iteration training features
- train_labels: per-iteration training labels
- test_list.txt: file listing the directories containing the feature maps for
                 the test images, useful for debugging purposes
- train_list.txt: file listing the directories containing the feature maps for
                  the train images, useful for debugging purposes
                  
Concerning the AVG_stats directory, it should contain six files when the
simulation ends correctly:
- AUC.txt:
	1st value: Area Under Curve, averaged over the dataset and the different
                   random iterations
	2nd value: standard deviation for the 1st value
- F.txt:
	1st value: F-measure, averaged over the dataset and the different random
                   iterations
        2nd value: standard deviation for the 1st value
- PR.txt:
	1st column: recall values
	2nd column: average precision values
	3rd column: standard deviation for the precision values
	4th column: average minus standard deviation (useful for plotting
		    uncertainty)
	5th column: average plus standard deviation (useful for plotting
	            uncertainty)
- RI.txt:
	1st value: Rand Index, averaged over the dataset and the different
		   random iterations
	2nd value: standard deviation for the 1st value
- ROC.txt:
	1st column: FPR values
	2nd column: average TPR values
	3rd column: standard deviation for the TPR values
	4th column: average minus standard deviation (useful for plotting
		    uncertainty)
	5th column: average plus standard deviation (useful for plotting
	            uncertainty)
- VI.txt:
	1st value: Variation of Information, averaged over the dataset and the
                   different random iterations
	2nd value: standard deviation for the 1st value

