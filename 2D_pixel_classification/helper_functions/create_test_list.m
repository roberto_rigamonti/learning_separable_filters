function [] = create_test_list(p)
%  create_test_list  Create a file containing the list of files corresponding
%                    to test feature maps
%
%  Synopsis:
%     create_test_list(p)
%
%  Input:
%     p = structure containing system's configuration and paths

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

fd = fopen(p.paths.test_fm_list,'wt');
for i_test_img = 1:p.test_imgs_no
    img_filename = p.test_imgs_list{i_test_img};
    [img_path,img_name,img_ext] = fileparts(img_filename); %#ok<*NASGU,ASGLU>
    fm_output_dir = fullfile(p.paths.test_fm_dir,img_name);
    fprintf(fd,'%s\n',fm_output_dir);
end
fclose(fd);

end
