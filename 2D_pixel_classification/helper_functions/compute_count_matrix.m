function [count_matrix] = compute_count_matrix(bin_1,bin_2)
%  compute_count_matrix  Compute the count matrix used for VI/RI computation
%
%  Algorithm taken from the code published by Allen Y. Yang
%  http://www.eecs.berkeley.edu/~yang/software/lossy_segmentation/
%
%  Synopsis:
%     [count_matrix] = compute_count_matrix(bin_1,bin_2)
%
%  Input:
%     bin1 = first binary image
%     bin2 = second binary image
%  Output:
%     count_matrix = computed count matrix

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

count_matrix = zeros(2,2);
count_matrix(1,1) = sum(~bin_1(:)&~bin_2(:));
count_matrix(1,2) = sum(~bin_1(:)&bin_2(:));
count_matrix(2,1) = sum(bin_1(:)&~bin_2(:));
count_matrix(2,2) = sum(bin_1(:)&bin_2(:));

end
