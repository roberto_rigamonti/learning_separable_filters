/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef DATASET_HH
#define DATASET_HH

#include <iostream>
#include <string>
#include "opencv2/opencv.hpp"
#include <boost/shared_ptr.hpp>

#include "parameters.hh"
#include "exceptions.hh"

class Dataset {
public:
  Dataset() { };
  virtual ~Dataset() { };

  virtual void parse_dataset_config(const std::string& dataset_config_filename) = 0;
  virtual cv::Mat get_sample() const = 0;

  virtual void get_sample_dimensions(struct sample_dimensions& dims) const = 0;

protected:
  mutable cv::RNG m_rng;
  std::string m_dataset_name;
};

struct sample_dimensions {
  unsigned int dims_no;
  std::vector< unsigned int > user_sizes;
  std::vector< unsigned int > extended_sizes;
};

#endif // DATASET_HH
