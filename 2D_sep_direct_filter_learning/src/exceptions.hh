/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef EXCEPTIONS_HH
#define EXCEPTIONS_HH

#include <iostream>
#include <stdexcept>
#include <cstring>

#define EXCEPTION_MSG_SIZE 256

class ConfigParseException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  ConfigParseException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class PathException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  PathException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class DatasetException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  DatasetException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class FilterBankException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  FilterBankException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class WFilterException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  WFilterException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class FileException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  FileException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class ProcessingException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  ProcessingException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class SimulationException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  SimulationException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class ResumeException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  ResumeException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

class NotImplException : public std::exception {
private:
  char message_[EXCEPTION_MSG_SIZE];
public:
  NotImplException(const char* msg) {
	strncpy(message_,msg,EXCEPTION_MSG_SIZE);
  }

  virtual const char* what() const throw() {
	return(message_);
  }
};

#endif // EXCEPTIONS_HH
