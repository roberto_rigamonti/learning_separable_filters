/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef SPARSEIT_2D_NUCLEAR_SS_HH
#define SPARSEIT_2D_NUCLEAR_SS_HH

#include <iostream>
#include <string>
#include "opencv2/opencv.hpp"
#include <boost/shared_ptr.hpp>
#include <omp.h>
#include <csignal>

#include "../cmd_line.hh"
#include "../filter_bank.hh"
#include "../dataset.hh"
#include "../optimization.hh"
#include "../parameters.hh"
#include "../exceptions.hh"
#include "../utils.hh"
#include "sparseit_SS.hh"

extern sig_atomic_t terminating_signal;

class SparseIT_2D_NUCLEAR_SS : public SparseIT_2D_SS {
public:
  SparseIT_2D_NUCLEAR_SS(CmdLine& cmd_line, Parameters& config, boost::shared_ptr< Filter_bank >& filters, boost::shared_ptr< Dataset >& dataset);
  virtual ~SparseIT_2D_NUCLEAR_SS() { };

  void optimize_filters();
  void compute_statistics(const unsigned int iteration_number);
protected:
  void reduce_rank(std::vector< cv::Mat >& filters_gradient) const;
  void parse_optimization_config(const std::string& opt_algo_config_filename);
  void evaluate_obj_function(const cv::Mat& residual, double& l1_penalty, double& l2_rec_error, double& nuclear_penalty, double& avg_rank, double& obj_fun_value) const;
  float get_filter_rank(const unsigned int filter_number) const;
  float get_nuclear_penalty(const unsigned int filter_number) const;

  float m_lambda_nuclear;
};

#endif // SPARSEIT_2D_NUCLEAR_SS_HH
