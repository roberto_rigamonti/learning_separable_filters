/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "sparseit_nuclear_SS.hh"

SparseIT_2D_NUCLEAR_SS::SparseIT_2D_NUCLEAR_SS(CmdLine& cmd_line, Parameters& config,
											   boost::shared_ptr< Filter_bank >& filters, boost::shared_ptr< Dataset >& dataset) {
  m_filters = filters;
  m_dataset = dataset;
  parse_optimization_config(config.get_opt_algo_config_filename());

  m_results_directory = config.get_results_directory();

  m_iteration_number = 0;
  if(cmd_line.resume())
	m_iteration_number = cmd_line.get_resumed_it_number();

  struct fb_dimensions fb_dims;
  struct sample_dimensions sample_dims;
  m_filters->get_fb_dimensions(fb_dims);
  m_dataset->get_sample_dimensions(sample_dims);
  // Assume rows == cols for both filters and samples (it's the SS version)
  m_filters_no = fb_dims.filters_no;
  m_filters_size = fb_dims.dims.at(0);
  m_sample_size = sample_dims.user_sizes.at(0);
  m_extended_sample_size = sample_dims.extended_sizes.at(0);
  // Compute useful quantities
  m_fm_size = m_extended_sample_size-m_filters_size+1; // Use valid correlations on twice-extended data
  m_rec_size = m_fm_size-m_filters_size+1;             // The second correlation gets the reconstruction

  std::cerr << "  Pre-loading feature maps and supporting structures" << std::endl;
  m_feature_maps.clear();
  for(unsigned int i_fm=0; i_fm<m_filters_no; ++i_fm) {
	cv::Mat tmp_map(m_fm_size, m_fm_size, CV_32FC1);
	m_feature_maps.push_back(tmp_map);
  }
}

void SparseIT_2D_NUCLEAR_SS::parse_optimization_config(const std::string& opt_algo_config_filename) {
  po::options_description cfg_file_descr("Optimization algorithm configuration file parameters");
  cfg_file_descr.add_options()
	("eta_coeffs", po::value< float >(&(m_eta_coeffs)),
	 "Gradient step for the coefficients")
	("eta_filters", po::value< float >(&(m_eta_filters)),
	 "Stochastic Gradient Descent step for the filters")
	("lambda_learn", po::value< float >(&(m_lambda_learn)),
	 "Regularization parameter for l1 penalty")
	("lambda_nuclear", po::value< float >(&(m_lambda_nuclear)),
	 "Regularization parameter for nuclear norm penalty")
	("coeffs_n_it", po::value< unsigned int >(&(m_coeffs_n_it)),
	 "Number of iterations for Iterative Thresholding (coefficients' estimation)")
	("filters_n_it", po::value< unsigned int >(&(m_filters_n_it)),
	 "Number of iterations for Stochastic Gradient Descent (filters' estimation)")
	("batch_size", po::value< unsigned int >(&(m_batch_size)),
	 "Size of a batch (set over which the filters' gradient is averaged before update)")
	("penalize_similar_filters", po::value< bool >(&(m_penalize_similar_filters)),
	 "Add a term to the energy function which penalizes the presence of similar filters")
	("xi_filters", po::value< float >(&(m_xi_filters)),
	 "Regularization parameter for the filter similarity term")
	("compute_statistics", po::value< bool >(&(m_compute_statistics)),
	 "At each interval, compute the statistics over the test set")
	("statistics_interval", po::value< unsigned int >(&(m_statistics_interval))->default_value(0),
	 "Size of the interval used to compute statistics")
	("test_set_cardinality", po::value< unsigned int >(&(m_test_set_cardinality)),
	 "Cardinality of the test set over which statistics are computed");

  // Parse config file
  std::ifstream ifs(opt_algo_config_filename.c_str());
  if(!ifs.is_open()) {
	throw ConfigParseException("Unable to read the optimization algorithm configuration file");
  }
  po::variables_map cfg_file_vm;
  po::store(po::parse_config_file(ifs, cfg_file_descr), cfg_file_vm);
  po::notify(cfg_file_vm);
  ifs.close();

  if(m_compute_statistics && m_statistics_interval<1) {
    throw ConfigParseException("Invalid statistics interval specified");
  }
}

void SparseIT_2D_NUCLEAR_SS::compute_statistics(const unsigned int iteration_number) {
  double l1_penalty = 0,l2_rec_error = 0,nuclear_penalty = 0,obj_fun_value = 0, avg_rank = 0;
  double tmp_l1_penalty,tmp_l2_rec_error,tmp_obj_fun_value,tmp_nuclear_penalty, tmp_avg_rank;
  if(m_test_set.empty()) {
	for(unsigned int i_stat=0; i_stat<m_test_set_cardinality; ++i_stat) {
	  cv::Mat sample = m_dataset->get_sample();
	  m_test_set.push_back(sample);
	}
  }
  for(unsigned int i_stat=0; i_stat<m_test_set_cardinality; ++i_stat) {
	cv::Mat sample = m_test_set.at(i_stat);
	compute_feature_maps(sample);

	cv::Mat reconstruction = compute_restricted_reconstruction();
	cv::Mat sample_roi = sample(cv::Range(m_filters_size, sample.rows-(m_filters_size)),
								cv::Range(m_filters_size, sample.cols-(m_filters_size)));
	cv::Mat residual = sample_roi-reconstruction;
	evaluate_obj_function(residual, tmp_l1_penalty, tmp_l2_rec_error, tmp_nuclear_penalty, tmp_avg_rank, tmp_obj_fun_value);
	l1_penalty += tmp_l1_penalty/m_test_set_cardinality;
	nuclear_penalty += tmp_nuclear_penalty/m_test_set_cardinality;
	l2_rec_error += tmp_l2_rec_error/m_test_set_cardinality;
	avg_rank += tmp_avg_rank/m_test_set_cardinality;
	obj_fun_value += tmp_obj_fun_value/m_test_set_cardinality;
  }
  std::cerr << "  [iteration " << iteration_number << " ] - statistics computed on " << m_test_set_cardinality << " test samples:" << std::endl;
  std::cerr << "    l1_penalty    = " << l1_penalty << std::endl;
  std::cerr << "    nuclear_penalty    = " << nuclear_penalty << std::endl;
  std::cerr << "    avg_rank    = " << avg_rank/m_filters_no << std::endl;
  std::cerr << "    l2_rec_error  = " << l2_rec_error << std::endl;
  std::cerr << "    obj_fun_value = " << obj_fun_value << " (lambda_learn = " << m_lambda_learn << ", lambda_nuclear=" << m_lambda_nuclear << ")" <<std::endl;

  char stat_filename[MAX_FILENAME_SIZE];
  snprintf(stat_filename, MAX_FILENAME_SIZE, "%s/statistics.txt", m_results_directory.c_str());
  std::ofstream stat_fd(stat_filename, std::fstream::app);
  if(!stat_fd.is_open()) {
	throw FileException("Unable to open the statistics file");
  }
  stat_fd << m_iteration_number << " " << l1_penalty << " " << l2_rec_error << " " << nuclear_penalty << " " << avg_rank/m_filters_no << " "<< obj_fun_value << " " << m_lambda_learn << std::endl;
  stat_fd.close();
}

float SparseIT_2D_NUCLEAR_SS::get_filter_rank(const unsigned int filter_number) const {
  cv::Mat U, Ss, VT;
  cv::Mat filter = *(m_filters->element_at(filter_number));
  float rank = 0;
  cv::SVD::compute(filter, Ss, U, VT);
  for(int rc = 0; rc < Ss.rows; ++rc) {
	float *Mr = Ss.ptr< float >(rc);
	if(fabs(Mr[0]) > 1e-6) {
	  ++rank;
	}
  }
  return(rank);
}

float SparseIT_2D_NUCLEAR_SS::get_nuclear_penalty(const unsigned int filter_number) const {
  cv::Mat U, Ss, VT;
  cv::Mat filter = *(m_filters->element_at(filter_number));
  float n_p = 0;
  cv::SVD::compute(filter, Ss, U, VT);
  for(int rc = 0; rc < Ss.rows; ++rc) {
	float *Mr = Ss.ptr< float >(rc);
	n_p += Mr[0];
  }
  return(n_p);
}

void SparseIT_2D_NUCLEAR_SS::evaluate_obj_function(const cv::Mat& residual, double& l1_penalty, double& l2_rec_error, double& nuclear_penalty, double& avg_rank, double& obj_fun_value) const {
  l1_penalty = 0;
  nuclear_penalty = 0;
  avg_rank = 0;
  for(unsigned int i_fm=0; i_fm<m_filters_no; ++i_fm) {
	cv::Mat fm = m_feature_maps.at(i_fm);
	cv::Mat fm_roi = fm(cv::Range(m_filters_size/2+1, m_fm_size-(m_filters_size/2+1)), cv::Range(m_filters_size/2+1, m_fm_size-(m_filters_size/2+1)));
	l1_penalty += cv::norm(fm_roi, cv::NORM_L1);
	nuclear_penalty += get_nuclear_penalty(i_fm);
	avg_rank += get_filter_rank(i_fm);
  }
  l2_rec_error = cv::norm(residual, cv::NORM_L2);
  obj_fun_value = 0.5*l2_rec_error*l2_rec_error+m_lambda_learn*l1_penalty+m_lambda_nuclear*nuclear_penalty;
}

void SparseIT_2D_NUCLEAR_SS::optimize_filters() {
  // Create the vector of filter gradients that will be used throughout the optimization process
  std::vector< cv::Mat > filters_gradient;
  for(unsigned int i_filter=0; i_filter<m_filters_no; ++i_filter) {
	cv::Mat tmp_gradient(m_filters_size, m_filters_size, CV_32FC1, cv::Scalar(0));
	filters_gradient.push_back(tmp_gradient);
  }

  while(!terminating_signal) {
	cv::Mat sample = m_dataset->get_sample();

	for(unsigned int i_filters_iters=0; i_filters_iters<m_filters_n_it; ++i_filters_iters) {
	  reset_filter_gradients(filters_gradient);
	  for(unsigned int i_batch=0; i_batch<m_batch_size; ++i_batch) {
		compute_feature_maps(sample);
		compute_filters_gradients(sample, filters_gradient);
	  }
	  m_filters->update_filters(filters_gradient, m_batch_size);
	  // Perform rank reduction using the nuclear norm
	  reset_filter_gradients(filters_gradient);
	  reduce_rank(filters_gradient);
	  m_filters->update_filters(filters_gradient,1);
	}
	m_filters->store_filter_bank(++m_iteration_number);
	if(m_compute_statistics && m_iteration_number%m_statistics_interval == 0)
	  compute_statistics(m_iteration_number);
  }
}

void SparseIT_2D_NUCLEAR_SS::reduce_rank(std::vector< cv::Mat >& filters_gradient) const {
#pragma omp parallel for schedule(dynamic)
  for(unsigned int i_filter=0; i_filter<m_filters_no; ++i_filter) {
	cv::Mat filter = *(m_filters->element_at(i_filter));

	cv::Mat U, Ss, V, VT;
	cv::Mat squared_S(m_filters_size, m_filters_size, CV_32FC1, cv::Scalar(0));
	cv::SVD::compute(filter, Ss, U, VT);
	for(int rc = 0; rc < Ss.rows; ++rc) {
	  float *Mr = Ss.ptr< float >(rc);
	  float *SMMr = squared_S.ptr< float >(rc);

	  if(fabs(Mr[0]) < m_lambda_nuclear) {
		Mr[0] = 0;
	  }
	  if(Mr[0] >= m_lambda_nuclear)
		Mr[0] -= m_lambda_nuclear;
	  if(Mr[0] <= -m_lambda_nuclear)
		Mr[0] += m_lambda_nuclear;
	  SMMr[rc] = Mr[0];
	}
	cv::Mat tmp = U*squared_S;
	cv::Mat reconstructed_filter = tmp*VT;

	// The gradient has to be inverted (owing to the sign used in filters' update function)
	filters_gradient.at(i_filter) = reconstructed_filter-filter;
  }
}
