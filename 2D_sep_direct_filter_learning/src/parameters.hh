/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef PARAMETERS_HH
#define PARAMETERS_HH

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <boost/config.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "exceptions.hh"
#include "utils.hh"
#include "cmd_line.hh"

#define SYS_CONFIG_FNAME            "system_config.conf"
#define MAX_FILENAME_SIZE           256

namespace po = boost::program_options;
namespace fs = boost::filesystem;

class Parameters {
public:
  Parameters(const CmdLine& cmd_line);
  ~Parameters() {};

  int get_rand_seed() const { return(m_rand_seed); }
  std::string get_dataset_type() const { return(m_dataset_type); }
  std::string get_dataset_config_filename() const {	return(m_dataset_config_filename); }
  std::string get_filter_bank_type() const { return(m_filter_bank_type); }
  std::string get_filter_bank_config_filename() const {	return(m_filter_bank_config_filename); }
  std::string get_opt_algo() const { return(m_opt_algo); }
  std::string get_opt_algo_config_filename() const { return(m_opt_algo_config_filename); }
  std::string get_results_directory() const { return(m_results_directory); }

private:
  unsigned int m_rand_seed;
  // Algorithms and config files
  std::string m_dataset_type;
  std::string m_dataset_config_filename;
  std::string m_filter_bank_type;
  std::string m_filter_bank_config_filename;
  std::string m_opt_algo;
  std::string m_opt_algo_config_filename;
  std::string m_results_directory;
};

#endif // PARAMETERS_HH
