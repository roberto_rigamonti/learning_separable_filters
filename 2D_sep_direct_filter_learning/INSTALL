## Install instructions for the filter learning code ##

This code learns a separable filter bank on medical images, as described in [1].
This code has been tested under Ubuntu 12.04 (64 bit), and might have problems
on other architectures/operating systems. Please let me know should you
encounter any.

+ REQUIRED libraries
- OpenCV [2]
- Boost C++ library [3]
- OpenMP [4]

+ Compilation
Here is the usual procedure for compiling the code:
  mkdir build
  cd build
  cmake ..
  make -j 8
The final executable will be placed in the bin/ subdirectory.

+ Dataset configuration
Please follow the format used in the example included in the data/ subdirectory.

+ Use of the obtained filter banks
Please note that the saved filter banks have an header that has to be manually
removed before using them in the companion pixel classification code (ensure
that no newline is left on top).

For any question or bug report, please feel free to contact me at:
roberto <dot> rigamonti <at> epfl <dot> ch


[1] R. Rigamonti, A. Sironi, V. Lepetit, and P. Fua, "Learning Separable
    Filters", CVPR 2013
[2] http://http://opencv.willowgarage.com/
[3] http://www.boost.org/
[4] http://openmp.org/

